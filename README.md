# Beats

This is the *beats microservice*, implementing the [Swatch Internet Time][swatch_internet_time].

Yes the 90's are over, but the idea to divide the day in 1000 units
and abandon the concept of timezones continues to be awesome.

Primary goal is to display the current time on a webpage.
Besides, it can also distribute it via *mqtt* (topics below `time/beats`).

It was written as a simple application to discover and learn more
about the topics *websockets* and *mqtt* in general.

[swatch_internet_time]: https://en.wikipedia.org/wiki/Swatch_Internet_Time
