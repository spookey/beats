#!/bin/sh
#
# PROVIDE: beats
# REQUIRE: LOGIN
# KEYWORD: shutdown
#

. /etc/rc.subr

name=beats
rcvar=beats_enable

load_rc_config ${name}

: ${beats_enable:="NO"}
: ${beats_user:="duke"}
: ${beats_group:="duke"}
: ${beats_base_folder:="/usr/home/${beats_user}/run/${name}"}
: ${beats_application_jar:="${beats_base_folder}/${name}.jar"}
: ${beats_application_conf:="file:${beats_base_folder}/application.yaml"}
: ${beats_application_profiles:=""}
: ${beats_logging_conf:="classpath:/logback-spring.xml"}
: ${beats_logging_file:="/var/log/${beats_user}/${name}.log"}

logfile="/var/log/${name}.log"
pidfile="/var/run/${name}.pid"

procname="/usr/local/openjdk21/bin/java"
command="/usr/sbin/daemon"
command_args="\
  -f -T ${name} -t ${name} -o ${logfile} -p ${pidfile} \
  ${procname} -server \
  -Djava.net.preferIPv4Stack=false \
  -Djava.net.preferIPv6Addresses=true \
  -Dlogging.file.name=${beats_logging_file} \
  -Dlogging.config=${beats_logging_conf} \
  -Dspring.profiles.active=${beats_application_profiles} \
  -jar ${beats_application_jar} \
  --spring.config.additional-location=${beats_application_conf} \
"

extra_commands="reload"
start_precmd="beats_start_precmd"

beats_start_precmd() {
  install -o ${beats_user} -g ${beats_group} -m 644 -- /dev/null ${logfile}
  install -o ${beats_user} -g ${beats_group} -m 600 -- /dev/null ${pidfile}
}

run_rc_command "$1"
