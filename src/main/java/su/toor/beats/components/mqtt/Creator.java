package su.toor.beats.components.mqtt;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.util.Optional;
import org.eclipse.paho.mqttv5.client.MqttAsyncClient;
import org.eclipse.paho.mqttv5.client.MqttConnectionOptions;
import org.eclipse.paho.mqttv5.client.persist.MemoryPersistence;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanInstantiationException;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import su.toor.beats.configs.MqttConfig;
import su.toor.beats.configs.mqtt.sinks.BeatRawConfig;
import su.toor.beats.configs.mqtt.sinks.BeatStrConfig;

@Component
public class Creator {

    private static final Logger LOG = LoggerFactory.getLogger(Creator.class);

    static final String FIELD_TOPIC = "topic";

    @Bean
    public Boolean mqttDesired(final BeatRawConfig beatRawConfig, final BeatStrConfig beatStrConfig) {
        final var raw = Boolean.TRUE.equals(beatRawConfig.isEnabled());
        final var str = Boolean.TRUE.equals(beatStrConfig.isEnabled());

        if (raw) {
            LOG.info("raw beats enabled [{}]", keyValue(FIELD_TOPIC, beatRawConfig.getTopic()));
        }
        if (str) {
            LOG.info("str beats enabled [{}]", keyValue(FIELD_TOPIC, beatStrConfig.getTopic()));
        }

        return raw || str;
    }

    @Bean
    public MqttConnectionOptions mqttConnectionOptions(final MqttConfig mqttConfig, final Composer composer) {
        LOG.info("creating mqtt options");

        final var options = new MqttConnectionOptions();
        options.setCleanStart(mqttConfig.isCleanStart());
        options.setAutomaticReconnect(mqttConfig.isAutoReconnect());
        options.setRequestResponseInfo(true);
        options.setRequestProblemInfo(true);

        if (mqttConfig.hasCredentials()) {
            LOG.info(
                    "using mqtt credentials [{}] [{}]",
                    keyValue("username", mqttConfig.getUsername()),
                    keyValue("password", mqttConfig.getPassword()));

            options.setUserName(mqttConfig.getUsername());
            options.setPassword(mqttConfig.getPasswordBytes());
        }

        Optional.ofNullable(composer.willMessage()).ifPresent(will -> {
            LOG.info("register will message");
            options.setWill(will.topic(), will.mqttMessage());
        });

        return options;
    }

    @Bean
    public MqttAsyncClient mqttAsyncClient(final MqttConfig mqttConfig) {
        try {
            return new MqttAsyncClient(mqttConfig.getServerUri(), mqttConfig.getClientId(), new MemoryPersistence());
        } catch (final IllegalArgumentException | MqttException ex) {
            throw new BeanInstantiationException(Creator.class, "client creation failed", ex);
        }
    }
}
