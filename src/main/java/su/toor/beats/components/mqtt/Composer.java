package su.toor.beats.components.mqtt;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import su.toor.beats.configs.mqtt.sinks.AbstractSink;
import su.toor.beats.configs.mqtt.sinks.AbstractStatusSink;
import su.toor.beats.configs.mqtt.sinks.BeatRawConfig;
import su.toor.beats.configs.mqtt.sinks.BeatStrConfig;
import su.toor.beats.configs.mqtt.sinks.BirthStatusConfig;
import su.toor.beats.configs.mqtt.sinks.WillStatusConfig;

@Component
public class Composer {

    private final BeatRawConfig beatRawConfig;
    private final BeatStrConfig beatStrConfig;
    private final BirthStatusConfig birthStatusConfig;
    private final WillStatusConfig willStatusConfig;

    @Autowired
    public Composer(
            final BeatRawConfig beatRawConfig,
            final BeatStrConfig beatStrConfig,
            final BirthStatusConfig birthStatusConfig,
            final WillStatusConfig willStatusConfig) {
        this.beatRawConfig = beatRawConfig;
        this.beatStrConfig = beatStrConfig;
        this.birthStatusConfig = birthStatusConfig;
        this.willStatusConfig = willStatusConfig;
    }

    public record Bundle(String topic, MqttMessage mqttMessage) {}

    @Nullable private static <T extends AbstractSink<T>> Bundle make(final AbstractSink<T> sink, final byte[] payload) {
        if (!sink.isEnabled()) {
            return null;
        }

        final var qos = sink.getQos();
        final var mqttMessage = new MqttMessage(payload, qos.ordinal(), sink.isRetained(), null);

        return new Bundle(sink.getTopic(), mqttMessage);
    }

    @Nullable private static <T extends AbstractStatusSink<T>> Bundle make(final AbstractStatusSink<T> sink) {
        return make(sink, sink.getMessageBytes());
    }

    @Nullable public Bundle birthMessage() {
        return make(birthStatusConfig);
    }

    @Nullable public Bundle willMessage() {
        return make(willStatusConfig);
    }

    @Nullable public Bundle beatRawMessage(final Integer beat) {
        final var buffer = ByteBuffer.allocate(Integer.BYTES)
                .order(ByteOrder.BIG_ENDIAN)
                .putInt(beat)
                .rewind();
        return make(beatRawConfig, buffer.array());
    }

    @Nullable public Bundle beatStrMessage(final Integer beat) {
        final var beatStr = "%03d".formatted(beat);
        return make(beatStrConfig, beatStr.getBytes(StandardCharsets.UTF_8));
    }
}
