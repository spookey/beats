package su.toor.beats.components.mqtt;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.util.Objects;
import java.util.Optional;
import org.eclipse.paho.mqttv5.client.MqttAsyncClient;
import org.eclipse.paho.mqttv5.client.MqttConnectionOptions;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.lang.Nullable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import su.toor.beats.core.events.BeatEvent;

@Component
public class Handler {

    private static final Logger LOG = LoggerFactory.getLogger(Handler.class);

    @Nullable private final MqttAsyncClient mqttAsyncClient;

    private final Composer composer;

    @Autowired
    public Handler(
            final MqttAsyncClient mqttAsyncClient,
            final MqttConnectionOptions mqttConnectionOptions,
            final ConnectActionListener connectActionListener,
            final Boolean mqttDesired,
            final Composer composer) {
        this.mqttAsyncClient = connect(mqttAsyncClient, mqttConnectionOptions, connectActionListener, mqttDesired);
        this.composer = composer;
    }

    static MqttAsyncClient connect(
            final MqttAsyncClient mqttAsyncClient,
            final MqttConnectionOptions mqttConnectionOptions,
            final ConnectActionListener connectActionListener,
            final Boolean mqttDesired) {
        if (Boolean.FALSE.equals(mqttDesired)) {
            return null;
        }
        if (mqttAsyncClient.isConnected()) {
            return mqttAsyncClient;
        }

        LOG.info("connecting to mqtt server [{}]", keyValue("serverUri", mqttAsyncClient.getServerURI()));
        try {
            final var token = mqttAsyncClient.connect(mqttConnectionOptions, null, connectActionListener);
            token.waitForCompletion();

            return mqttAsyncClient;
        } catch (final MqttException ex) {
            LOG.error("connection failed", ex);
        }
        return null;
    }

    static void send(@Nullable final MqttAsyncClient client, @Nullable final Composer.Bundle bundle) {
        if (Objects.isNull(bundle)) {
            LOG.warn("empty bundle passed");
            return;
        }
        if (Optional.ofNullable(client).filter(MqttAsyncClient::isConnected).isEmpty()) {
            LOG.warn("empty client or unconnected client passed");
            return;
        }

        try {
            client.publish(bundle.topic(), bundle.mqttMessage());
        } catch (final MqttException ex) {
            LOG.error(
                    "publishing failed [{}] [{}]",
                    keyValue("topic", bundle.topic()),
                    keyValue("message", bundle.mqttMessage()),
                    ex);
        }
    }

    @Async
    @EventListener(BeatEvent.class)
    public void provideBeat(final BeatEvent beatEvent) {
        final var value = beatEvent.getValue();

        send(mqttAsyncClient, composer.beatRawMessage(value));
        send(mqttAsyncClient, composer.beatStrMessage(value));
    }
}
