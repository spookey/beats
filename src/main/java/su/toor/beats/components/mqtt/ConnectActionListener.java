package su.toor.beats.components.mqtt;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.util.Optional;
import org.eclipse.paho.mqttv5.client.IMqttToken;
import org.eclipse.paho.mqttv5.client.MqttActionListener;
import org.eclipse.paho.mqttv5.client.MqttAsyncClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConnectActionListener implements MqttActionListener {

    private static final Logger LOG = LoggerFactory.getLogger(ConnectActionListener.class);

    static final String FIELD_TOKEN = "token";

    private final Composer composer;

    @Autowired
    public ConnectActionListener(final Composer composer) {
        this.composer = composer;
    }

    void sendBirthMessage(final MqttAsyncClient mqttAsyncClient) {
        Optional.ofNullable(composer.birthMessage()).ifPresent(birth -> {
            LOG.info("sending birth message");
            Handler.send(mqttAsyncClient, birth);
        });
    }

    @Override
    public void onSuccess(final IMqttToken asyncActionToken) {
        LOG.info("connection success [{}]", keyValue(FIELD_TOKEN, asyncActionToken));

        Optional.ofNullable(asyncActionToken.getClient())
                .map(MqttAsyncClient.class::cast)
                .ifPresent(this::sendBirthMessage);
    }

    @Override
    public void onFailure(final IMqttToken asyncActionToken, final Throwable exception) {
        LOG.error("connection failure [{}]", keyValue(FIELD_TOKEN, asyncActionToken), exception);
    }
}
