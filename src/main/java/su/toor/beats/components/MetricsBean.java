package su.toor.beats.components;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import java.util.Map;
import java.util.function.ToDoubleFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import su.toor.beats.core.BeatService;

@Component
public class MetricsBean {

    private static final Logger LOG = LoggerFactory.getLogger(MetricsBean.class);

    static final String BEATS_GAUGE_NAME = "internet_time";
    static final String BEATS_GAUGE_DESCRIPTION = "Current beats value";
    static final String BEATS_GAUGE_TAG_TYPE = "type";
    static final String BEATS_GAUGE_TAG_NAME = "name";

    private final MeterRegistry meterRegistry;
    private final BeatService beatService;

    @Autowired
    public MetricsBean(final MeterRegistry meterRegistry, final BeatService beatService) {
        this.meterRegistry = meterRegistry;
        this.beatService = beatService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void registerMetrics() {
        LOG.info("registering gauges for beats");

        final ToDoubleFunction<BeatService> beats = bean -> bean.getDto().getBeat();
        final ToDoubleFunction<BeatService> deci = bean -> bean.getDto().getDeciBeat();
        final ToDoubleFunction<BeatService> centi = bean -> bean.getDto().getCentiBeat();
        final ToDoubleFunction<BeatService> milli = bean -> bean.getDto().getMilliBeat();

        Map.of(
                        "beats", beats,
                        "deci", deci,
                        "centi", centi,
                        "milli", milli)
                .forEach((type, func) -> Gauge.builder(BEATS_GAUGE_NAME, beatService, func)
                        .tag(BEATS_GAUGE_TAG_TYPE, type)
                        .tag(BEATS_GAUGE_TAG_NAME, "beats")
                        .description(BEATS_GAUGE_DESCRIPTION)
                        .register(meterRegistry));
    }
}
