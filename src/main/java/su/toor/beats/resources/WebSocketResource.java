package su.toor.beats.resources;

import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import su.toor.beats.configs.spring.WebSocketConfiguration;
import su.toor.beats.core.events.MilliBeatEvent;

@Timed
@Controller
public class WebSocketResource {

    static final String DESTINATION = WebSocketConfiguration.TOPIC_DESTINATION + "/beats";

    private final SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public WebSocketResource(final SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @Async
    @EventListener(MilliBeatEvent.class)
    public void provideBeat(final MilliBeatEvent milliBeatEvent) {
        simpMessagingTemplate.convertAndSend(DESTINATION, milliBeatEvent.getValue());
    }
}
