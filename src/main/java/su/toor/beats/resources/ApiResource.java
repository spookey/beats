package su.toor.beats.resources;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import io.micrometer.core.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import su.toor.beats.core.BeatDto;
import su.toor.beats.core.BeatService;

@Timed
@RestController
@RequestMapping(value = "/api", produces = APPLICATION_JSON_VALUE)
public class ApiResource {

    private static final Logger LOG = LoggerFactory.getLogger(ApiResource.class);

    private final BeatService beatService;

    @Autowired
    public ApiResource(final BeatService beatService) {
        this.beatService = beatService;
    }

    @Timed
    @GetMapping(value = "/beats", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<BeatDto> getBeats() {

        LOG.info("beats requested");

        final var result = beatService.getDto();
        return ResponseEntity.ok(result);
    }
}
