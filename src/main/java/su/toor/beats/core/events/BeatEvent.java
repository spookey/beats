package su.toor.beats.core.events;

public class BeatEvent extends AbstractEvent<Integer> {

    public BeatEvent(final Object source, final Integer value) {
        super(source, value);
    }
}
