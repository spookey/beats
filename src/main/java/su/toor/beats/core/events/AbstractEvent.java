package su.toor.beats.core.events;

import org.springframework.context.ApplicationEvent;

abstract class AbstractEvent<T extends Number> extends ApplicationEvent {

    private final T value;

    protected AbstractEvent(final Object source, final T value) {
        super(source);
        this.value = value;
    }

    public T getValue() {
        return value;
    }
}
