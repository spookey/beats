package su.toor.beats.core.events;

public class MilliBeatEvent extends AbstractEvent<Float> {

    public MilliBeatEvent(final Object source, final Float value) {
        super(source, value);
    }
}
