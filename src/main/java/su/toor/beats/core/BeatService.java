package su.toor.beats.core;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import su.toor.beats.core.events.BeatEvent;
import su.toor.beats.core.events.MilliBeatEvent;

@Service
@EnableScheduling
public class BeatService {

    private static final Logger LOG = LoggerFactory.getLogger(BeatService.class);

    static final long INTERVAL_REFRESH_INITIAL = 1000L;
    static final long INTERVAL_REFRESH_REGULAR = 50L;

    private final ApplicationEventPublisher applicationEventPublisher;

    private final BeatDto current = new BeatDto(0d);

    @Autowired
    public BeatService(final ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public BeatDto getDto() {
        return current;
    }

    @Scheduled(initialDelay = INTERVAL_REFRESH_INITIAL, fixedRate = INTERVAL_REFRESH_REGULAR)
    public void update() {
        final var value = Make.create();
        final var future = new BeatDto(value);

        if (current.milliBeatsChange(future)) {
            applicationEventPublisher.publishEvent(new MilliBeatEvent(this, future.getMilliBeat()));
        }
        if (current.beatsChange(future)) {
            final var beat = future.getBeat();
            applicationEventPublisher.publishEvent(new BeatEvent(this, beat));

            LOG.info("current [{}]", keyValue("beat", beat));
        }

        current.set(future);
    }

    protected static final class Make {
        private Make() {}

        static OffsetDateTime shift(final Instant instant) {
            return OffsetDateTime.ofInstant(instant, ZoneOffset.UTC).plusHours(1);
        }

        static double calculate(final int hrs, final int min, final int sec, final int nan) {
            return ((hrs * 3600) + (min * 60) + sec + (nan / 1_000_000_000d)) / 86.4d;
        }

        static double convert(final Instant instant) {
            final var bmt = shift(instant);
            return calculate(bmt.getHour(), bmt.getMinute(), bmt.getSecond(), bmt.getNano());
        }

        public static double create() {
            return convert(Instant.now());
        }
    }
}
