package su.toor.beats.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.Objects;
import java.util.function.DoubleFunction;
import org.springframework.core.style.ToStringCreator;
import su.toor.beats.core.misc.Require;

@JsonPropertyOrder({"beat", "deciBeat", "centiBeat", "milliBeat"})
public final class BeatDto {

    private double value;

    private final DoubleFunction<Double> round = places -> StrictMath.floor(value * places) / places;

    BeatDto(final double value) {
        this.set(value);
    }

    @JsonIgnore
    BeatDto set(final BeatDto dto) {
        return set(dto.value);
    }

    @JsonIgnore
    BeatDto set(final double value) {
        this.value = Require.betweenInExcluding("value", value, 0d, 1000d);
        return this;
    }

    @JsonProperty("beat")
    public int getBeat() {
        return round.apply(1d).intValue();
    }

    @JsonProperty("deciBeat")
    public float getDeciBeat() {
        return round.apply(10d).floatValue();
    }

    @JsonProperty("centiBeat")
    public float getCentiBeat() {
        return round.apply(100d).floatValue();
    }

    @JsonProperty("milliBeat")
    public float getMilliBeat() {
        return round.apply(1000d).floatValue();
    }

    @JsonIgnore
    boolean beatsChange(final BeatDto other) {
        return value != 0d && getBeat() != other.getBeat();
    }

    @JsonIgnore
    boolean milliBeatsChange(final BeatDto other) {
        return value != 0d && getMilliBeat() != other.getMilliBeat();
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof final BeatDto that)) {
            return false;
        }

        return this.value == that.value;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("beat", getBeat())
                .append("deciBeat", getDeciBeat())
                .append("centiBeat", getCentiBeat())
                .append("milliBeat", getMilliBeat())
                .toString();
    }
}
