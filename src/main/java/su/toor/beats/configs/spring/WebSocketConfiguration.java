package su.toor.beats.configs.spring;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@EnableWebSocketMessageBroker
@Configuration
public class WebSocketConfiguration implements WebSocketMessageBrokerConfigurer {

    public static final String TOPIC_DESTINATION = "/topic";
    static final String BEATS_ENDPOINT = "/beats-ws";

    static final String ALLOWED_ORIGIN_PATTERN = "http://localhost:[*]";
    static final String CLIENT_LIBRARY_URL = "../lib/sockjs-client/dist/sockjs.min.js";

    @Override
    public void configureMessageBroker(final MessageBrokerRegistry messageBrokerRegistry) {
        messageBrokerRegistry.enableSimpleBroker(TOPIC_DESTINATION);
    }

    @Override
    public void registerStompEndpoints(final StompEndpointRegistry stompEndpointRegistry) {
        stompEndpointRegistry
                .addEndpoint(BEATS_ENDPOINT)
                .setAllowedOriginPatterns(ALLOWED_ORIGIN_PATTERN)
                .withSockJS()
                .setClientLibraryUrl(CLIENT_LIBRARY_URL);
    }
}
