package su.toor.beats.configs;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.function.Predicate;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "mqtt")
@Validated
public class MqttConfig {

    private static final Random RND = new SecureRandom();

    static final String DEFAULT_SERVER_URI = "tcp://localhost:1883";
    static final String DEFAULT_CLIENT_ID_PREFIX = "beats";
    static final boolean DEFAULT_CLEAN_START = true;
    static final boolean DEFAULT_AUTO_RECONNECT = true;

    private String serverUri;
    private String clientId;
    private Boolean cleanStart;
    private Boolean autoReconnect;
    private String username;
    private String password;

    @NonNull public String getServerUri() {
        return Optional.ofNullable(serverUri).orElse(DEFAULT_SERVER_URI);
    }

    public MqttConfig setServerUri(final String serverUri) {
        this.serverUri = serverUri;
        return this;
    }

    @NonNull public String getClientId() {
        return Optional.ofNullable(clientId)
                .filter(Predicate.not(String::isEmpty))
                .orElseGet(() -> {
                    final var suffix = RND.nextInt(1000, 10000);
                    return "%s-%04d".formatted(DEFAULT_CLIENT_ID_PREFIX, suffix);
                });
    }

    public MqttConfig setClientId(final String clientId) {
        this.clientId = clientId;
        return this;
    }

    @NonNull public Boolean isCleanStart() {
        return Optional.ofNullable(cleanStart).orElse(DEFAULT_CLEAN_START);
    }

    public MqttConfig setCleanStart(final Boolean cleanStart) {
        this.cleanStart = cleanStart;
        return this;
    }

    @NonNull public Boolean isAutoReconnect() {
        return Optional.ofNullable(autoReconnect).orElse(DEFAULT_AUTO_RECONNECT);
    }

    public MqttConfig setAutoReconnect(final Boolean autoReconnect) {
        this.autoReconnect = autoReconnect;
        return this;
    }

    @Nullable public String getUsername() {
        return username;
    }

    public MqttConfig setUsername(final String username) {
        this.username = username;
        return this;
    }

    @Nullable public String getPassword() {
        return Optional.ofNullable(password)
                .map(pass -> "*".repeat(pass.length()))
                .orElse(null);
    }

    @Nullable public byte[] getPasswordBytes() {
        return Optional.ofNullable(password)
                .map(pass -> pass.getBytes(StandardCharsets.UTF_8))
                .orElse(null);
    }

    public MqttConfig setPassword(final String password) {
        this.password = password;
        return this;
    }

    public boolean hasCredentials() {
        return Objects.nonNull(username) && Objects.nonNull(password);
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("serverUri", getServerUri())
                .append("clientId", getClientId())
                .append("cleanStart", isCleanStart())
                .append("autoReconnect", isAutoReconnect())
                .append("username", getUsername())
                .append("password", getPassword())
                .toString();
    }
}
