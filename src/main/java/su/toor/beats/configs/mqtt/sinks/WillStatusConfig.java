package su.toor.beats.configs.mqtt.sinks;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "mqtt.sinks.will")
@Validated
public class WillStatusConfig extends AbstractStatusSink<WillStatusConfig> {

    public WillStatusConfig() {
        super(DEFAULT_TOPIC, DEFAULT_WILL_MESSAGE);
    }

    @Override
    protected WillStatusConfig get() {
        return this;
    }
}
