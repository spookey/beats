package su.toor.beats.configs.mqtt.sinks;

import java.nio.charset.StandardCharsets;
import java.util.Optional;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.NonNull;
import su.toor.beats.configs.mqtt.Qos;

public abstract class AbstractStatusSink<T extends AbstractStatusSink<T>> extends AbstractSink<AbstractStatusSink<T>> {

    static final String DEFAULT_TOPIC = "status/beats";
    static final String DEFAULT_BIRTH_MESSAGE = "Beats are alive";
    static final String DEFAULT_WILL_MESSAGE = "Beats are gone";

    private final String defaultMessage;

    private String message;

    protected AbstractStatusSink(@NonNull final String defaultTopic, @NonNull final String defaultMessage) {
        super(defaultTopic);
        this.defaultMessage = defaultMessage;
    }

    @Override
    protected abstract T get();

    @NonNull public String getMessage() {
        return Optional.ofNullable(message).orElse(defaultMessage);
    }

    @NonNull public byte[] getMessageBytes() {
        return getMessage().getBytes(StandardCharsets.UTF_8);
    }

    public T setMessage(final String message) {
        this.message = message;
        return get();
    }

    @Override
    public T setEnabled(final Boolean enabled) {
        super.setEnabled(enabled);
        return get();
    }

    @Override
    public T setTopic(final String topic) {
        super.setTopic(topic);
        return get();
    }

    @Override
    public T setQos(final Qos qos) {
        super.setQos(qos);
        return get();
    }

    @Override
    public T setRetained(final Boolean retained) {
        super.setRetained(retained);
        return get();
    }

    @Override
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator().append("message", getMessage());
    }
}
