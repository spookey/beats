package su.toor.beats.configs.mqtt.sinks;

import java.util.Optional;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.NonNull;
import su.toor.beats.configs.mqtt.Qos;
import su.toor.beats.core.misc.Require;

public abstract class AbstractSink<T extends AbstractSink<T>> {

    static final boolean DEFAULT_ENABLED = false;
    static final String DEFAULT_RAW_TOPIC = "time/beats/raw";
    static final String DEFAULT_STR_TOPIC = "time/beats/str";
    static final Qos DEFAULT_QOS = Qos.ZERO;
    static final boolean DEFAULT_RETAINED = false;

    private final String defaultTopic;

    private Boolean enabled;
    private String topic;
    private Qos qos;
    private Boolean retained;

    protected AbstractSink(final String defaultTopic) {
        this.defaultTopic = Require.notNull("defaultTopic", defaultTopic);
    }

    protected abstract T get();

    @NonNull public Boolean isEnabled() {
        return Optional.ofNullable(enabled).orElse(DEFAULT_ENABLED);
    }

    public T setEnabled(final Boolean enabled) {
        this.enabled = enabled;
        return get();
    }

    @NonNull public String getTopic() {
        return Optional.ofNullable(topic).orElse(defaultTopic);
    }

    public T setTopic(final String topic) {
        this.topic = topic;
        return get();
    }

    @NonNull public Qos getQos() {
        return Optional.ofNullable(qos).orElse(DEFAULT_QOS);
    }

    public T setQos(final Qos qos) {
        this.qos = qos;
        return get();
    }

    @NonNull public Boolean isRetained() {
        return Optional.ofNullable(retained).orElse(DEFAULT_RETAINED);
    }

    public T setRetained(final Boolean retained) {
        this.retained = retained;
        return get();
    }

    protected ToStringCreator toStringCreator() {
        return new ToStringCreator(this)
                .append("enabled", isEnabled())
                .append("topic", getTopic())
                .append("qos", getQos())
                .append("retained", isRetained());
    }

    @Override
    public String toString() {
        return toStringCreator().toString();
    }
}
