package su.toor.beats.configs.mqtt;

public enum Qos {
    ZERO,
    ONE,
    TWO
}
