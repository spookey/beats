package su.toor.beats.configs.mqtt.sinks;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "mqtt.sinks.beat-str")
@Validated
public class BeatStrConfig extends AbstractSink<BeatStrConfig> {

    public BeatStrConfig() {
        super(DEFAULT_STR_TOPIC);
    }

    @Override
    protected BeatStrConfig get() {
        return this;
    }
}
