package su.toor.beats.configs.mqtt.sinks;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "mqtt.sinks.beat-raw")
@Validated
public class BeatRawConfig extends AbstractSink<BeatRawConfig> {

    public BeatRawConfig() {
        super(DEFAULT_RAW_TOPIC);
    }

    @Override
    protected BeatRawConfig get() {
        return this;
    }
}
