package su.toor.beats.configs.mqtt.sinks;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "mqtt.sinks.birth")
@Validated
public class BirthStatusConfig extends AbstractStatusSink<BirthStatusConfig> {

    public BirthStatusConfig() {
        super(DEFAULT_TOPIC, DEFAULT_BIRTH_MESSAGE);
    }

    @Override
    protected BirthStatusConfig get() {
        return this;
    }
}
