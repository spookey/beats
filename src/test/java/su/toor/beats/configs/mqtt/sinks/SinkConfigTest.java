package su.toor.beats.configs.mqtt.sinks;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import su.toor.beats.configs.mqtt.Qos;

class SinkConfigTest {

    private static Stream<Arguments> sinkConfigs() {
        return Stream.of(
                Arguments.of(AbstractSink.DEFAULT_RAW_TOPIC, new BeatRawConfig()),
                Arguments.of(AbstractSink.DEFAULT_STR_TOPIC, new BeatStrConfig()));
    }

    private static <T extends AbstractSink<T>> void verifyConfig(
            final AbstractSink<T> config,
            final Boolean enabled,
            final String topic,
            final Qos qos,
            final Boolean retained) {
        assertThat(config.isEnabled()).isEqualTo(enabled);
        assertThat(config.getTopic()).isEqualTo(topic);
        assertThat(config.getQos()).isEqualTo(qos);
        assertThat(config.isRetained()).isEqualTo(retained);
    }

    @ParameterizedTest
    @MethodSource(value = "sinkConfigs")
    <T extends AbstractSink<T>> void empty(final String topic, final AbstractSink<T> config) {
        verifyConfig(
                config, AbstractSink.DEFAULT_ENABLED, topic, AbstractSink.DEFAULT_QOS, AbstractSink.DEFAULT_RETAINED);
    }

    @ParameterizedTest
    @MethodSource(value = "sinkConfigs")
    <T extends AbstractSink<T>> void some(final String ignore, final AbstractSink<T> config) {
        final var enabled = false;
        final var topic = "some/beat/topic";
        final var qos = Qos.ONE;
        final var retained = true;

        config.setEnabled(enabled).setTopic(topic).setQos(qos).setRetained(retained);

        verifyConfig(config, enabled, topic, qos, retained);

        assertThat(config.toString())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .contains(String.valueOf(enabled))
                .contains(topic)
                .contains(String.valueOf(qos))
                .contains(String.valueOf(retained));
    }
}
