package su.toor.beats.configs.mqtt.sinks;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.beats.core.misc.RequireAssert.assertThatRequireException;

import java.nio.charset.StandardCharsets;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import su.toor.beats.configs.mqtt.Qos;

class StatusSinkConfigTest {

    private static Stream<Arguments> statusConfigs() {
        return Stream.of(
                Arguments.of(AbstractStatusSink.DEFAULT_BIRTH_MESSAGE, new BirthStatusConfig()),
                Arguments.of(AbstractStatusSink.DEFAULT_WILL_MESSAGE, new WillStatusConfig()));
    }

    private static <T extends AbstractStatusSink<T>> void verifyConfig(
            final AbstractStatusSink<T> config,
            final Boolean enabled,
            final String topic,
            final Qos qos,
            final Boolean retained,
            final String message) {
        assertThat(config.isEnabled()).isEqualTo(enabled);
        assertThat(config.getTopic()).isEqualTo(topic);
        assertThat(config.getQos()).isEqualTo(qos);
        assertThat(config.isRetained()).isEqualTo(retained);
        assertThat(config.getMessage()).isEqualTo(message);
        assertThat(config.getMessageBytes()).isEqualTo(message.getBytes(StandardCharsets.UTF_8));
    }

    @ParameterizedTest
    @MethodSource(value = "statusConfigs")
    <T extends AbstractStatusSink<T>> void empty(final String message, final AbstractStatusSink<T> config) {
        verifyConfig(
                config,
                AbstractStatusSink.DEFAULT_ENABLED,
                AbstractStatusSink.DEFAULT_TOPIC,
                AbstractStatusSink.DEFAULT_QOS,
                AbstractStatusSink.DEFAULT_RETAINED,
                message);
    }

    @ParameterizedTest
    @MethodSource(value = "statusConfigs")
    <T extends AbstractStatusSink<T>> void some(final String ignore, final AbstractStatusSink<T> config) {
        final var enabled = true;
        final var topic = "some/status/topic";
        final var message = "Custom status message";
        final var qos = Qos.TWO;
        final var retained = true;

        config.setEnabled(enabled)
                .setTopic(topic)
                .setQos(qos)
                .setRetained(retained)
                .setMessage(message);

        verifyConfig(config, enabled, topic, qos, retained, message);

        assertThat(config.toString())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .contains(String.valueOf(enabled))
                .contains(topic)
                .contains(String.valueOf(qos))
                .contains(String.valueOf(retained))
                .contains(message);
    }

    private static class DefaultTopicNullSink extends AbstractSink<DefaultTopicNullSink> {
        DefaultTopicNullSink() {
            super(null);
        }

        @Override
        protected DefaultTopicNullSink get() {
            return this;
        }
    }

    @Test
    void defaultTopicNull() {
        assertThatRequireException().isThrownBy(DefaultTopicNullSink::new);
    }
}
