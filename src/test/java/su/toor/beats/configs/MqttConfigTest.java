package su.toor.beats.configs;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.StandardCharsets;
import org.junit.jupiter.api.Test;

class MqttConfigTest {

    private static void verifyConnection(
            final MqttConfig config,
            final String serverUri,
            final String clientId,
            final Boolean cleanStart,
            final Boolean autoReconnect) {
        assertThat(config.getServerUri()).isEqualTo(serverUri);
        if (clientId == null) {
            assertThat(config.getClientId())
                    .startsWith(MqttConfig.DEFAULT_CLIENT_ID_PREFIX)
                    .matches("^.+-[0-9]{4}$");
        } else {
            assertThat(config.getClientId()).isEqualTo(clientId);
        }
        assertThat(config.isCleanStart()).isEqualTo(cleanStart);
        assertThat(config.isAutoReconnect()).isEqualTo(autoReconnect);
    }

    private static void verifyCredentials(final MqttConfig config, final String username, final String password) {
        assertThat(config.getUsername()).isEqualTo(username);
        if (password == null) {
            assertThat(config.getPassword()).isNull();
            assertThat(config.getPasswordBytes()).isNull();
            assertThat(config.hasCredentials()).isFalse();
        } else {
            assertThat(config.getPassword()).isEqualTo("*".repeat(password.length()));
            assertThat(config.getPasswordBytes()).isEqualTo(password.getBytes(StandardCharsets.UTF_8));
            assertThat(config.hasCredentials()).isEqualTo(username != null);
        }
    }

    @Test
    void empty() {
        verifyConnection(
                new MqttConfig(),
                MqttConfig.DEFAULT_SERVER_URI,
                null,
                MqttConfig.DEFAULT_CLEAN_START,
                MqttConfig.DEFAULT_AUTO_RECONNECT);

        verifyConnection(
                new MqttConfig().setClientId(""),
                MqttConfig.DEFAULT_SERVER_URI,
                null,
                MqttConfig.DEFAULT_CLEAN_START,
                MqttConfig.DEFAULT_AUTO_RECONNECT);

        verifyCredentials(new MqttConfig(), null, null);
    }

    @Test
    void credentials() {
        final var username = "some-user";
        final var password = "security-1337!";

        verifyCredentials(new MqttConfig(), null, null);
        verifyCredentials(new MqttConfig().setUsername(username), username, null);
        verifyCredentials(new MqttConfig().setPassword(password), null, password);
        verifyCredentials(new MqttConfig().setUsername(username).setPassword(password), username, password);
    }

    @Test
    void some() {
        final var serverUri = "tcp://mqtt.example.org";
        final var clientId = "awesome-test-client";
        final var cleanStart = true;
        final var autoReconnect = false;
        final var username = "the-user-name";
        final var password = "very-secure-password-1";

        final var config = new MqttConfig()
                .setServerUri(serverUri)
                .setClientId(clientId)
                .setCleanStart(cleanStart)
                .setAutoReconnect(autoReconnect)
                .setUsername(username)
                .setPassword(password);

        verifyConnection(config, serverUri, clientId, cleanStart, autoReconnect);
        verifyCredentials(config, username, password);

        assertThat(config.toString())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .contains(serverUri)
                .contains(clientId)
                .contains(String.valueOf(cleanStart))
                .contains(String.valueOf(autoReconnect))
                .contains(username)
                .contains("*".repeat(password.length()));
    }
}
