package su.toor.beats.configs.spring;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.core.task.AsyncTaskExecutor;

class TaskConfigurationTest {

    private static final TaskConfiguration TASK_CONFIGURATION = new TaskConfiguration();

    @Test
    void taskExecutor() {
        final var executor = TASK_CONFIGURATION.taskExecutor();

        assertThat(executor).isInstanceOf(AsyncTaskExecutor.class);
    }
}
