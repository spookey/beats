package su.toor.beats.configs.spring;

import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.socket.config.annotation.SockJsServiceRegistration;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.StompWebSocketEndpointRegistration;

@ExtendWith(SpringExtension.class)
class WebSocketConfigurationTest {

    @MockitoBean
    private MessageBrokerRegistry messageBrokerRegistry;

    @MockitoBean
    private StompEndpointRegistry stompEndpointRegistry;

    @MockitoBean
    private StompWebSocketEndpointRegistration stompWebSocketEndpointRegistration;

    @MockitoBean
    private SockJsServiceRegistration sockJsServiceRegistration;

    @Test
    void configure() {
        final var config = new WebSocketConfiguration();

        assertThatNoException().isThrownBy(() -> config.configureMessageBroker(messageBrokerRegistry));

        verify(messageBrokerRegistry, times(1)).enableSimpleBroker(WebSocketConfiguration.TOPIC_DESTINATION);
        verifyNoMoreInteractions(messageBrokerRegistry);
    }

    @Test
    void register() {
        when(stompEndpointRegistry.addEndpoint(any())).thenReturn(stompWebSocketEndpointRegistration);

        when(stompWebSocketEndpointRegistration.setAllowedOriginPatterns(any()))
                .thenReturn(stompWebSocketEndpointRegistration);

        when(stompWebSocketEndpointRegistration.withSockJS()).thenReturn(sockJsServiceRegistration);

        when(sockJsServiceRegistration.setClientLibraryUrl(anyString())).thenReturn(sockJsServiceRegistration);

        final var config = new WebSocketConfiguration();
        config.registerStompEndpoints(stompEndpointRegistry);

        verify(stompEndpointRegistry, times(1)).addEndpoint(WebSocketConfiguration.BEATS_ENDPOINT);
        verifyNoMoreInteractions(stompEndpointRegistry);

        verify(stompEndpointRegistry, times(1)).addEndpoint(WebSocketConfiguration.BEATS_ENDPOINT);
        verifyNoMoreInteractions(stompEndpointRegistry);

        verify(stompWebSocketEndpointRegistration, times(1))
                .setAllowedOriginPatterns(WebSocketConfiguration.ALLOWED_ORIGIN_PATTERN);
        verify(stompWebSocketEndpointRegistration, times(1)).withSockJS();
        verifyNoMoreInteractions(stompWebSocketEndpointRegistration);

        verify(sockJsServiceRegistration, times(1)).setClientLibraryUrl(WebSocketConfiguration.CLIENT_LIBRARY_URL);
        verifyNoMoreInteractions(sockJsServiceRegistration);
    }
}
