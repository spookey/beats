package su.toor.beats.resources;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.URI;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.beats.core.BeatCreator;
import su.toor.beats.core.BeatService;

@WebMvcTest(ApiResource.class)
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
class ApiResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockitoBean
    private BeatService beatService;

    private static URI beatsUri() {
        return UriComponentsBuilder.fromPath("/api/beats").build().toUri();
    }

    @Test
    void getBeats() throws Exception {
        final var beatDto = BeatCreator.dto(13.12d);

        when(beatService.getDto()).thenReturn(beatDto);

        mockMvc.perform(get(beatsUri()).accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", isA(Map.class)))
                .andExpect(jsonPath("$.beat", allOf(isA(Integer.class), equalTo(beatDto.getBeat()))))
                .andExpect(jsonPath("$.deciBeat", allOf(isA(Double.class), closeTo(beatDto.getDeciBeat(), 0.00001d))))
                .andExpect(jsonPath("$.centiBeat", allOf(isA(Double.class), closeTo(beatDto.getCentiBeat(), 0.00001d))))
                .andExpect(
                        jsonPath("$.milliBeat", allOf(isA(Double.class), closeTo(beatDto.getMilliBeat(), 0.00001d))));

        verify(beatService, times(1)).getDto();
    }
}
