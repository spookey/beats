package su.toor.beats.resources;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.beats.core.events.MilliBeatEvent;

@ExtendWith(SpringExtension.class)
class WebSocketResourceTest {

    @MockitoBean
    private SimpMessagingTemplate simpMessagingTemplate;

    @Test
    void provide() {
        final var event = new MilliBeatEvent(WebSocketResourceTest.class, 13.12f);

        doAnswer(invocation -> {
                    final var destination = invocation.getArgument(0, String.class);
                    final var payload = invocation.getArgument(1, Float.class);

                    assertThat(destination).isEqualTo(WebSocketResource.DESTINATION);
                    assertThat(payload).isEqualTo(event.getValue());

                    return null;
                })
                .when(simpMessagingTemplate)
                .convertAndSend(WebSocketResource.DESTINATION, event.getValue());

        final var resource = new WebSocketResource(simpMessagingTemplate);
        resource.provideBeat(event);

        verify(simpMessagingTemplate, times(1)).convertAndSend(WebSocketResource.DESTINATION, event.getValue());
        verifyNoMoreInteractions(simpMessagingTemplate);
    }
}
