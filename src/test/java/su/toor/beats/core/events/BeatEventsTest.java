package su.toor.beats.core.events;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import su.toor.beats.core.BeatCreator;
import su.toor.beats.core.BeatDto;

class BeatEventsTest {

    private static final BeatDto BEAT_DTO = BeatCreator.dto(13.12d);

    private static Stream<Arguments> events() {
        final var beat = BEAT_DTO.getBeat();
        final var milliBeat = BEAT_DTO.getMilliBeat();

        return Stream.of(
                Arguments.of(new MilliBeatEvent(BeatEventsTest.class, milliBeat), milliBeat),
                Arguments.of(new BeatEvent(BeatEventsTest.class, beat), beat));
    }

    @ParameterizedTest
    @MethodSource(value = "events")
    <T extends Number> void similar(final AbstractEvent<T> event, final T value) {
        assertThat(event.getValue()).isEqualTo(value);
        assertThat(event.getSource()).isSameAs(BeatEventsTest.class);
        assertThat(event.getTimestamp()).isNotZero();
    }
}
