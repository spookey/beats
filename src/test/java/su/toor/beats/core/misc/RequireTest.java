package su.toor.beats.core.misc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static su.toor.beats.core.misc.RequireAssert.assertThatRequireException;

import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

class RequireTest {

    @Nested
    class RequireExceptionTest {

        @ParameterizedTest
        @ValueSource(strings = {"error", "message"})
        @NullAndEmptySource
        void create(final String message) {
            assertThat(new Require.RequireException(message)).hasMessage(message);
        }
    }

    @Test
    void condition() {
        final var message = "custom error message";
        final var value = "content";

        assertThatIllegalArgumentException()
                .isThrownBy(() -> Require.condition(null, message, 42))
                .withMessage("check is null");
        assertThatIllegalArgumentException()
                .isThrownBy(() -> Require.condition(Objects::nonNull, null, 23))
                .withMessage("message is null, empty or blank");
        assertThatIllegalArgumentException()
                .isThrownBy(() -> Require.condition(Objects::nonNull, "", 42))
                .withMessage("message is null, empty or blank");
        assertThatIllegalArgumentException()
                .isThrownBy(() -> Require.condition(Objects::nonNull, "\n", 23))
                .withMessage("message is null, empty or blank");

        assertThatRequireException()
                .isThrownBy(() -> Require.condition(String::isEmpty, message, value))
                .withMessage(message);

        assertThat(Require.condition(Predicate.not(String::isEmpty), message, value))
                .isEqualTo(value);
    }

    @Test
    void notNull() {
        final var fieldName = "field";
        final var text = "text";

        assertThatRequireException()
                .isThrownBy(() -> Require.notNull(fieldName, null))
                .withMessage(fieldName + " is null");

        assertThat(Require.notNull(fieldName, text)).isEqualTo(text);
    }

    @Test
    void betweenInExcluding() {
        final var fieldName = "some";

        assertThatRequireException()
                .isThrownBy(() -> Require.betweenInExcluding(null, 0d, 42d, 23d))
                .withMessage("fieldName is null");

        assertThatRequireException()
                .isThrownBy(() -> Require.betweenInExcluding(fieldName, null, 23d, 42d))
                .withMessage(fieldName + ".value is null");

        assertThatRequireException()
                .isThrownBy(() -> Require.betweenInExcluding(fieldName, 0d, null, 42d))
                .withMessage(fieldName + ".lowerIncluding is null");

        assertThatRequireException()
                .isThrownBy(() -> Require.betweenInExcluding(fieldName, 0d, 23d, null))
                .withMessage(fieldName + ".upperExcluding is null");

        assertThatRequireException()
                .isThrownBy(() -> Require.betweenInExcluding(fieldName, 0d, 42d, 23d))
                .withMessage("Depp!");
        assertThatRequireException()
                .isThrownBy(() -> Require.betweenInExcluding(fieldName, 0d, 23d, 23d))
                .withMessage("Depp!");

        assertThatRequireException()
                .isThrownBy(() -> Require.betweenInExcluding(fieldName, 0d, 23d, 42d))
                .withMessage(fieldName + " is too low");

        assertThatRequireException()
                .isThrownBy(() -> Require.betweenInExcluding(fieldName, 42d, 23d, 42d))
                .withMessage(fieldName + " is too high");

        IntStream.range(23, 42).boxed().map(Number::doubleValue).forEach(value -> assertThat(
                        Require.betweenInExcluding(fieldName, value, 23d, 42d))
                .isEqualTo(value));
    }
}
