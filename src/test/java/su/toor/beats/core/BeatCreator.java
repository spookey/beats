package su.toor.beats.core;

public final class BeatCreator {
    private BeatCreator() {}

    public static BeatDto dto(final double value) {
        return new BeatDto(value);
    }
}
