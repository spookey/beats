package su.toor.beats.core;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.beats.core.BeatService.Make;
import su.toor.beats.core.events.BeatEvent;
import su.toor.beats.core.events.MilliBeatEvent;

@ExtendWith(SpringExtension.class)
class BeatServiceTest {

    private static final BeatDto EMPTY_DTO = BeatCreator.dto(0d);

    @MockitoBean
    private ApplicationEventPublisher applicationEventPublisher;

    @Test
    void constants() {
        assertThat(BeatService.INTERVAL_REFRESH_INITIAL).isPositive();
        assertThat(BeatService.INTERVAL_REFRESH_REGULAR)
                .isPositive()
                .isLessThanOrEqualTo(BeatService.INTERVAL_REFRESH_INITIAL);
    }

    @Test
    void dto() {
        final var beatService = new BeatService(applicationEventPublisher);

        final var dto = beatService.getDto();

        assertThat(dto.getCentiBeat()).isZero();
        assertThat(dto.getBeat()).isZero();
        assertThat(dto).isEqualTo(EMPTY_DTO).isSameAs(dto);
    }

    @Test
    void noUpdate() {
        try (final var mockedStatic = mockStatic(Make.class)) {
            mockedStatic.when(Make::create).thenReturn(0.0001d).thenReturn(0.0002d);

            final var service = new BeatService(applicationEventPublisher);
            assertThat(service.getDto()).isEqualTo(EMPTY_DTO);

            service.update();
            assertThat(service.getDto()).isNotEqualTo(EMPTY_DTO).isEqualTo(BeatCreator.dto(0.0001d));

            service.update();
            assertThat(service.getDto()).isNotEqualTo(EMPTY_DTO).isEqualTo(BeatCreator.dto(0.0002d));

            mockedStatic.verify(Make::create, times(2));
        }

        verifyNoInteractions(applicationEventPublisher);
    }

    @Test
    void fastUpdate() {
        try (final var mockedStatic = mockStatic(Make.class)) {
            mockedStatic.when(Make::create).thenReturn(1.0d).thenReturn(1.01d);

            final var service = new BeatService(applicationEventPublisher);
            assertThat(service.getDto()).isEqualTo(EMPTY_DTO);

            service.update();
            assertThat(service.getDto()).isNotEqualTo(EMPTY_DTO).isEqualTo(BeatCreator.dto(1.0d));

            service.update();
            assertThat(service.getDto()).isNotEqualTo(EMPTY_DTO).isEqualTo(BeatCreator.dto(1.01d));

            mockedStatic.verify(Make::create, times(2));
        }

        verify(applicationEventPublisher, times(1)).publishEvent(any(MilliBeatEvent.class));
        verifyNoMoreInteractions(applicationEventPublisher);
    }

    @Test
    void slowUpdate() {
        try (final var mockedStatic = mockStatic(Make.class)) {
            mockedStatic.when(Make::create).thenReturn(1.0d).thenReturn(2.01d);

            final var service = new BeatService(applicationEventPublisher);
            assertThat(service.getDto()).isEqualTo(EMPTY_DTO);

            service.update();
            assertThat(service.getDto()).isNotEqualTo(EMPTY_DTO).isEqualTo(BeatCreator.dto(1.0d));

            service.update();
            assertThat(service.getDto()).isNotEqualTo(EMPTY_DTO).isEqualTo(BeatCreator.dto(2.01d));

            mockedStatic.verify(Make::create, times(2));
        }

        verify(applicationEventPublisher, times(1)).publishEvent(any(MilliBeatEvent.class));
        verify(applicationEventPublisher, times(1)).publishEvent(any(BeatEvent.class));
        verifyNoMoreInteractions(applicationEventPublisher);
    }
}
