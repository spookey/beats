package su.toor.beats.core;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class BeatServiceMakeTest {

    private static final double FRACTION = 1000d / (24d * 60d * 60d);
    private static final Offset<Double> OFFSET = Offset.offset(0.000_009d);

    private static double calc(final int hrs, final int min, final int sec, final int nan) {
        return (hrs * 60d * 60d * FRACTION)
                + (min * 60d * FRACTION)
                + (sec * FRACTION)
                + (nan / 1_000_000_000d * FRACTION);
    }

    @Test
    void shift() {
        final var start = OffsetDateTime.now(ZoneOffset.UTC);
        final var shift = start.plusHours(1);

        final var result = BeatService.Make.shift(start.toInstant());
        assertThat(result.getOffset()).isEqualTo(ZoneOffset.UTC);
        assertThat(result.toInstant()).isAfter(start.toInstant());

        assertThat(result.getYear()).isEqualTo(shift.getYear());
        assertThat(result.getMonth()).isEqualTo(shift.getMonth());
        assertThat(result.getDayOfYear()).isEqualTo(shift.getDayOfYear());
        assertThat(result.getDayOfMonth()).isEqualTo(shift.getDayOfMonth());
        assertThat(result.getHour()).isEqualTo(shift.getHour());
        assertThat(result.getMinute()).isEqualTo(shift.getMinute());
        assertThat(result.getSecond()).isEqualTo(shift.getSecond());
        assertThat(result.getNano()).isEqualTo(shift.getNano());
    }

    private static Stream<Arguments> exactValues() {
        return Stream.of(
                Arguments.of(0, 0d),
                Arguments.of(3, 125d),
                Arguments.of(6, 250d),
                Arguments.of(9, 375d),
                Arguments.of(12, 500d),
                Arguments.of(15, 625d),
                Arguments.of(18, 750d),
                Arguments.of(21, 875d));
    }

    @ParameterizedTest
    @MethodSource(value = "exactValues")
    void calculateExact(final Integer hrs, final Double expect) {
        assertThat(BeatService.Make.calculate(hrs, 0, 0, 0)).isCloseTo(expect, OFFSET);
    }

    @Test
    void calculate() {
        IntStream.rangeClosed(0, 23).forEach(hrs -> IntStream.rangeClosed(0, 59)
                .forEach(min -> IntStream.rangeClosed(0, 59).forEach(sec -> {
                    final var expect = calc(hrs, min, sec, 0);
                    assertThat(BeatService.Make.calculate(hrs, min, sec, 0))
                            .as("%02d:%02d:%02d".formatted(hrs, min, sec))
                            .isNotNegative()
                            .isCloseTo(expect, OFFSET);
                })));
    }

    @Test
    void convert() {
        final var start = OffsetDateTime.now(ZoneOffset.UTC);
        final var shift = start.plusHours(1);
        final var expect = calc(shift.getHour(), shift.getMinute(), shift.getSecond(), shift.getNano());

        final var result = BeatService.Make.convert(start.toInstant());

        assertThat(result).isNotNegative().isCloseTo(expect, OFFSET);
    }

    @Test
    void create() {
        final var start = OffsetDateTime.now(ZoneOffset.UTC);
        final var shift = start.plusHours(1);
        final var expect = calc(shift.getHour(), shift.getMinute(), shift.getSecond(), shift.getNano());
        final var offset = Offset.offset(0.1d);

        final var result = BeatService.Make.create();

        assertThat(result).isNotNegative().isCloseTo(expect, offset);
    }
}
