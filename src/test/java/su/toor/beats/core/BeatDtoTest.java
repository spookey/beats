package su.toor.beats.core;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.beats.core.misc.RequireAssert.assertThatRequireException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

class BeatDtoTest {

    @ParameterizedTest
    @ValueSource(doubles = {-1d, 1000d})
    void invalid(final Double value) {
        assertThatRequireException().isThrownBy(() -> new BeatDto(value));

        final var dto = new BeatDto(0d);
        assertThatRequireException().isThrownBy(() -> dto.set(value));
    }

    @ParameterizedTest
    @ValueSource(doubles = {0d, 1d, 250d, 500d, 750d, 998d, 999d})
    void simple(final Double value) {
        final var dto = new BeatDto(value);

        assertThat(dto.getBeat()).isEqualTo(value.intValue());
        assertThat(dto.getDeciBeat()).isEqualTo(value.floatValue());
        assertThat(dto.getCentiBeat()).isEqualTo(value.floatValue());
        assertThat(dto.getMilliBeat()).isEqualTo(value.floatValue());
    }

    @ParameterizedTest
    @CsvSource(
            value = {
                "0.0000, 0.000, 0.00, 0.0, 0",
                "0.0010, 0.001, 0.00, 0.0, 0",
                "0.0100, 0.010, 0.01, 0.0, 0",
                "0.0101, 0.010, 0.01, 0.0, 0",
                "0.1010, 0.101, 0.10, 0.1, 0",
                "0.1110, 0.111, 0.11, 0.1, 0",
                "0.9999, 0.999, 0.99, 0.9, 0",
                "1.0000, 1.000, 1.00, 1.0, 1",
                "1.0001, 1.000, 1.00, 1.0, 1",
            })
    void fractions(final Double value, final Float milli, final Float centi, final Float deci, final Integer beat) {
        final var dto = new BeatDto(value);

        assertThat(dto.getBeat()).as("(%f) %d beat".formatted(value, beat)).isEqualTo(beat);

        assertThat(dto.getDeciBeat()).as("(%f) %f deci".formatted(value, deci)).isEqualTo(deci);

        assertThat(dto.getCentiBeat())
                .as("(%f) %f centi".formatted(value, centi))
                .isEqualTo(centi);

        assertThat(dto.getMilliBeat())
                .as("(%f) %f milli".formatted(value, milli))
                .isEqualTo(milli);
    }

    @Test
    void set() {
        final var nil = new BeatDto(0d);
        final var one = new BeatDto(1d);

        final var init = new BeatDto(0d);
        assertThat(init.getBeat()).isZero();
        assertThat(init.getDeciBeat()).isZero();
        assertThat(init.getCentiBeat()).isZero();
        assertThat(init.getMilliBeat()).isZero();

        final var some = init.set(nil);
        assertThat(some.getBeat()).isZero();
        assertThat(some.getDeciBeat()).isZero();
        assertThat(some.getCentiBeat()).isZero();
        assertThat(some.getMilliBeat()).isZero();

        final var fresh = some.set(one);
        assertThat(fresh.getBeat()).isOne();
        assertThat(fresh.getDeciBeat()).isOne();
        assertThat(fresh.getCentiBeat()).isOne();
        assertThat(fresh.getMilliBeat()).isOne();

        final var text = fresh.set(13.12d);
        assertThat(text.toString())
                .isNotBlank()
                .contains("beat")
                .contains("deci")
                .contains("centi")
                .contains("milli")
                .contains(String.valueOf(13.12d))
                .contains(String.valueOf(13));
    }

    @Test
    void change() {
        final var one = new BeatDto(1d);
        final var two = new BeatDto(2d);
        final var dot = new BeatDto(2.1d);

        assertThat(one.milliBeatsChange(one)).isFalse();
        assertThat(two.milliBeatsChange(two)).isFalse();
        assertThat(dot.milliBeatsChange(dot)).isFalse();

        assertThat(one.milliBeatsChange(two)).isTrue();
        assertThat(one.milliBeatsChange(dot)).isTrue();
        assertThat(two.milliBeatsChange(dot)).isTrue();

        assertThat(one.beatsChange(one)).isFalse();
        assertThat(two.beatsChange(two)).isFalse();
        assertThat(dot.beatsChange(dot)).isFalse();

        assertThat(one.beatsChange(two)).isTrue();
        assertThat(one.beatsChange(dot)).isTrue();
        assertThat(two.beatsChange(dot)).isFalse();
    }

    @Test
    void equality() {
        final var init = new BeatDto(0d);
        final var nil = new BeatDto(0d);
        final var one = new BeatDto(1d);

        assertThat(init)
                .hasSameHashCodeAs(nil)
                .doesNotHaveSameHashCodeAs(one)
                .hasSameHashCodeAs(init)
                .isNotSameAs(nil)
                .isNotSameAs(one)
                .isSameAs(init)
                .isEqualTo(nil)
                .isNotEqualTo(one)
                .isEqualTo(init)
                .isNotEqualTo(new Object());
    }
}
