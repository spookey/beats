package su.toor.beats.components;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.beats.core.BeatDto;
import su.toor.beats.core.BeatService;

@ExtendWith(SpringExtension.class)
class MetricsBeanTest {

    @Test
    void registration() {
        final var values = Map.of(
                "beats", 23f,
                "deci", 42.5f,
                "centi", 23.42f,
                "milli", 42.235f);
        final var dto = mock(BeatDto.class);
        when(dto.getBeat()).thenReturn(values.get("beats").intValue());
        when(dto.getDeciBeat()).thenReturn(values.get("deci"));
        when(dto.getCentiBeat()).thenReturn(values.get("centi"));
        when(dto.getMilliBeat()).thenReturn(values.get("milli"));

        final var registry = new SimpleMeterRegistry();
        final var service = mock(BeatService.class);
        when(service.getDto()).thenReturn(dto);

        final var metrics = new MetricsBean(registry, service);

        assertThat(registry.getMeters()).isEmpty();
        metrics.registerMetrics();

        assertThat(registry.getMeters()).hasSize(4);

        registry.forEachMeter(meter -> {
            assertThat(meter).isInstanceOf(Gauge.class);

            final var id = meter.getId();
            assertThat(id.getName()).isEqualTo(MetricsBean.BEATS_GAUGE_NAME);
            assertThat(id.getDescription()).isEqualTo(MetricsBean.BEATS_GAUGE_DESCRIPTION);

            final var nameTag = id.getTag(MetricsBean.BEATS_GAUGE_TAG_NAME);
            assertThat(nameTag).isNotNull().isEqualTo("beats");
            final var typeTag = id.getTag(MetricsBean.BEATS_GAUGE_TAG_TYPE);
            assertThat(typeTag).isNotNull().isIn("beats", "deci", "centi", "milli");

            final var measurement = meter.measure();
            assertThat(measurement).hasSize(1);
            measurement.forEach(item ->
                    assertThat(item.getValue()).isEqualTo(values.get(typeTag).doubleValue()));
        });
    }
}
