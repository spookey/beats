package su.toor.beats.components.mqtt;

import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import org.eclipse.paho.mqttv5.client.IMqttToken;
import org.eclipse.paho.mqttv5.client.MqttAsyncClient;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class ConnectActionListenerTest {

    @MockitoBean
    private Composer composer;

    @MockitoBean
    private IMqttToken iMqttToken;

    @MockitoBean
    private MqttAsyncClient mqttAsyncClient;

    @Test
    void sendBirthNoMessage() {
        when(composer.birthMessage()).thenReturn(null);

        final var listener = new ConnectActionListener(composer);

        assertThatNoException().isThrownBy(() -> listener.sendBirthMessage(mqttAsyncClient));

        verifyNoInteractions(mqttAsyncClient);
    }

    @Test
    void sendBirthMessage() throws MqttException {
        final var topic = "topic/birth";
        final var mqttMessage = new MqttMessage("message".getBytes(StandardCharsets.UTF_8));

        when(composer.birthMessage()).thenReturn(new Composer.Bundle(topic, mqttMessage));
        when(mqttAsyncClient.isConnected()).thenReturn(true);

        final var listener = new ConnectActionListener(composer);

        assertThatNoException().isThrownBy(() -> listener.sendBirthMessage(mqttAsyncClient));

        verify(mqttAsyncClient, times(1)).isConnected();
        verify(mqttAsyncClient, times(1)).publish(topic, mqttMessage);
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void sendBirthMessageFailed() throws MqttException {
        when(composer.birthMessage()).thenReturn(new Composer.Bundle("", new MqttMessage()));
        when(mqttAsyncClient.isConnected()).thenReturn(true);
        when(mqttAsyncClient.publish(anyString(), any(MqttMessage.class)))
                .thenThrow(new MqttException(new RuntimeException("test error")));

        final var listener = new ConnectActionListener(composer);

        assertThatNoException().isThrownBy(() -> listener.sendBirthMessage(mqttAsyncClient));

        verify(mqttAsyncClient, times(1)).isConnected();
        verify(mqttAsyncClient, times(1)).publish(anyString(), any(MqttMessage.class));
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void successNoClient() {
        when(iMqttToken.getClient()).thenReturn(null);

        final var listener = new ConnectActionListener(composer);

        assertThatNoException().isThrownBy(() -> listener.onSuccess(iMqttToken));

        verifyNoInteractions(mqttAsyncClient);
    }

    @Test
    void success() throws MqttException {
        when(mqttAsyncClient.isConnected()).thenReturn(true);
        when(iMqttToken.getClient()).thenAnswer(invocation -> mqttAsyncClient);
        when(composer.birthMessage()).thenReturn(new Composer.Bundle("", new MqttMessage()));

        final var listener = new ConnectActionListener(composer);

        assertThatNoException().isThrownBy(() -> listener.onSuccess(iMqttToken));

        verify(mqttAsyncClient, times(1)).isConnected();
        verify(mqttAsyncClient, times(1)).publish(anyString(), any(MqttMessage.class));
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void failure() {
        final var listener = new ConnectActionListener(composer);

        final var exception = new Exception();

        assertThatNoException().isThrownBy(() -> listener.onFailure(iMqttToken, exception));
    }
}
