package su.toor.beats.components.mqtt;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import su.toor.beats.configs.mqtt.Qos;
import su.toor.beats.configs.mqtt.sinks.BeatRawConfig;
import su.toor.beats.configs.mqtt.sinks.BeatStrConfig;
import su.toor.beats.configs.mqtt.sinks.BirthStatusConfig;
import su.toor.beats.configs.mqtt.sinks.WillStatusConfig;

class ComposerTest {

    @Test
    void disabled() {
        final var composer = new Composer(
                new BeatRawConfig().setEnabled(false),
                new BeatStrConfig().setEnabled(false),
                new BirthStatusConfig().setEnabled(false),
                new WillStatusConfig().setEnabled(false));

        Stream.of(
                        composer.beatRawMessage(0),
                        composer.beatStrMessage(0),
                        composer.birthMessage(),
                        composer.willMessage())
                .forEach(bundle -> assertThat(bundle).isNull());
    }

    private static void verify(
            final Composer.Bundle bundle,
            final String topic,
            final Qos qos,
            final Boolean retained,
            final Consumer<byte[]> payloadCheck) {
        assertThat(bundle).isNotNull();
        assertThat(bundle.topic()).isEqualTo(topic);

        final var message = bundle.mqttMessage();
        assertThat(message.getQos()).isEqualTo(qos.ordinal());
        assertThat(message.isRetained()).isEqualTo(retained);

        payloadCheck.accept(message.getPayload());
    }

    @Test
    void content() {
        final var beatRawTopic = "topic/beats/raw";
        final var beatStrTopic = "topic/beats/str";
        final var statusTopic = "topic/status";
        final var beatQos = Qos.ONE;
        final var statusQos = Qos.TWO;
        final var beatRetained = false;
        final var statusRetained = true;
        final var birthMessage = "Hello!";
        final var willMessage = "Bye!";
        final var beat = 999;

        final var composer = new Composer(
                new BeatRawConfig()
                        .setEnabled(true)
                        .setTopic(beatRawTopic)
                        .setQos(beatQos)
                        .setRetained(beatRetained),
                new BeatStrConfig()
                        .setEnabled(true)
                        .setTopic(beatStrTopic)
                        .setQos(beatQos)
                        .setRetained(beatRetained),
                new BirthStatusConfig()
                        .setEnabled(true)
                        .setTopic(statusTopic)
                        .setQos(statusQos)
                        .setRetained(statusRetained)
                        .setMessage(birthMessage),
                new WillStatusConfig()
                        .setEnabled(true)
                        .setTopic(statusTopic)
                        .setQos(statusQos)
                        .setRetained(statusRetained)
                        .setMessage(willMessage));

        verify(composer.beatRawMessage(beat), beatRawTopic, beatQos, beatRetained, payload -> {
            final var number = new BigInteger(payload).intValue();
            assertThat(number).isEqualTo(beat);
        });

        verify(composer.beatStrMessage(beat), beatStrTopic, beatQos, beatRetained, payload -> {
            final var text = new String(payload, StandardCharsets.UTF_8);
            assertThat(text).isEqualTo(String.valueOf(beat));
        });

        verify(composer.birthMessage(), statusTopic, statusQos, statusRetained, payload -> assertThat(payload)
                .isEqualTo(birthMessage.getBytes(StandardCharsets.UTF_8)));

        verify(composer.willMessage(), statusTopic, statusQos, statusRetained, payload -> assertThat(payload)
                .isEqualTo(willMessage.getBytes(StandardCharsets.UTF_8)));
    }
}
