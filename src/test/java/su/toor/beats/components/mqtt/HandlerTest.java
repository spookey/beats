package su.toor.beats.components.mqtt;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.eclipse.paho.mqttv5.client.MqttAsyncClient;
import org.eclipse.paho.mqttv5.client.MqttConnectionOptions;
import org.eclipse.paho.mqttv5.client.MqttToken;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockedStatic;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.beats.core.events.BeatEvent;

@ExtendWith(SpringExtension.class)
class HandlerTest {

    @MockitoBean
    private MqttAsyncClient mqttAsyncClient;

    @MockitoBean
    private MqttConnectionOptions mqttConnectionOptions;

    @MockitoBean
    private ConnectActionListener connectActionListener;

    @MockitoBean
    private Composer composer;

    @Test
    void connectNotDesired() {
        final var client = Handler.connect(mqttAsyncClient, mqttConnectionOptions, connectActionListener, false);

        assertThat(client).isNull();

        verifyNoInteractions(mqttAsyncClient);
    }

    @Test
    void connectAlreadyConnected() {
        when(mqttAsyncClient.isConnected()).thenReturn(true);

        final var client = Handler.connect(mqttAsyncClient, mqttConnectionOptions, connectActionListener, true);

        assertThat(client).isSameAs(mqttAsyncClient);

        verify(mqttAsyncClient, times(1)).isConnected();
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void connectSuccess() throws MqttException {
        when(mqttAsyncClient.isConnected()).thenReturn(false);
        when(mqttAsyncClient.getServerURI()).thenReturn("test-server");
        final var token = mock(MqttToken.class);

        when(mqttAsyncClient.connect(mqttConnectionOptions, null, connectActionListener))
                .thenReturn(token);

        final var client = Handler.connect(mqttAsyncClient, mqttConnectionOptions, connectActionListener, true);

        assertThat(client).isSameAs(mqttAsyncClient);

        verify(mqttAsyncClient, times(1)).isConnected();
        verify(mqttAsyncClient, times(1)).getServerURI();
        verify(mqttAsyncClient, times(1)).connect(mqttConnectionOptions, null, connectActionListener);
        verifyNoMoreInteractions(mqttAsyncClient);

        verify(token, times(1)).waitForCompletion();
        verifyNoMoreInteractions(token);
    }

    @Test
    void connectFailed() throws MqttException {
        when(mqttAsyncClient.isConnected()).thenReturn(false);
        when(mqttAsyncClient.getServerURI()).thenReturn("test-server");
        when(mqttAsyncClient.connect(mqttConnectionOptions, null, connectActionListener))
                .thenThrow(new MqttException(new RuntimeException("test error")));

        final var client = Handler.connect(mqttAsyncClient, mqttConnectionOptions, connectActionListener, true);

        assertThat(client).isNull();

        verify(mqttAsyncClient, times(1)).isConnected();
        verify(mqttAsyncClient, times(1)).getServerURI();
        verify(mqttAsyncClient, times(1)).connect(mqttConnectionOptions, null, connectActionListener);
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void sendNullParameters() {
        final var bundle = new Composer.Bundle("", new MqttMessage());

        assertThatNoException().isThrownBy(() -> Handler.send(null, null));
        assertThatNoException().isThrownBy(() -> Handler.send(mqttAsyncClient, null));
        assertThatNoException().isThrownBy(() -> Handler.send(null, bundle));

        verifyNoInteractions(mqttAsyncClient);
    }

    @Test
    void sendNotConnected() {
        final var bundle = new Composer.Bundle("", new MqttMessage());

        when(mqttAsyncClient.isConnected()).thenReturn(false);

        assertThatNoException().isThrownBy(() -> Handler.send(mqttAsyncClient, bundle));

        verify(mqttAsyncClient, times(1)).isConnected();
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void sendSuccess() throws MqttException {
        final var topic = "topic/some";
        final var mqttMessage = new MqttMessage();
        final var bundle = new Composer.Bundle(topic, mqttMessage);

        when(mqttAsyncClient.isConnected()).thenReturn(true);

        assertThatNoException().isThrownBy(() -> Handler.send(mqttAsyncClient, bundle));

        verify(mqttAsyncClient, times(1)).isConnected();
        verify(mqttAsyncClient, times(1)).publish(topic, mqttMessage);
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void sendFailed() throws MqttException {
        final var topic = "topic/success";
        final var mqttMessage = new MqttMessage();
        final var bundle = new Composer.Bundle(topic, mqttMessage);

        when(mqttAsyncClient.isConnected()).thenReturn(true);
        when(mqttAsyncClient.publish(topic, mqttMessage))
                .thenThrow(new MqttException(new RuntimeException("test error")));

        assertThatNoException().isThrownBy(() -> Handler.send(mqttAsyncClient, bundle));

        verify(mqttAsyncClient, times(1)).isConnected();
        verify(mqttAsyncClient, times(1)).publish(topic, mqttMessage);
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void provide() {
        final var event = new BeatEvent(HandlerTest.class, 0);

        final var rawBundle = new Composer.Bundle("some/raw/topic", new MqttMessage());
        final var strBundle = new Composer.Bundle("some/str/topic", new MqttMessage());

        when(composer.beatRawMessage(event.getValue())).thenReturn(rawBundle);
        when(composer.beatStrMessage(event.getValue())).thenReturn(strBundle);

        try (final MockedStatic<Handler> mockedStatic = mockStatic(Handler.class)) {
            mockedStatic
                    .when(() -> Handler.connect(
                            any(MqttAsyncClient.class),
                            any(MqttConnectionOptions.class),
                            any(ConnectActionListener.class),
                            anyBoolean()))
                    .thenReturn(mqttAsyncClient);

            final var handler =
                    new Handler(mqttAsyncClient, mqttConnectionOptions, connectActionListener, true, composer);

            assertThatNoException().isThrownBy(() -> handler.provideBeat(event));

            mockedStatic.verify(() -> Handler.send(eq(mqttAsyncClient), eq(rawBundle)));
            mockedStatic.verify(() -> Handler.send(eq(mqttAsyncClient), eq(strBundle)));
        }

        verify(composer, times(1)).beatRawMessage(event.getValue());
        verify(composer, times(1)).beatStrMessage(event.getValue());
        verifyNoMoreInteractions(composer);
    }
}
