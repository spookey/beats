package su.toor.beats.components.mqtt;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanInstantiationException;
import su.toor.beats.configs.MqttConfig;
import su.toor.beats.configs.mqtt.sinks.BeatRawConfig;
import su.toor.beats.configs.mqtt.sinks.BeatStrConfig;

class CreatorTest {

    private static Boolean createDesired(final boolean rawEnabled, final boolean strEnabled) {
        final var creator = new Creator();
        return creator.mqttDesired(
                new BeatRawConfig().setEnabled(rawEnabled), new BeatStrConfig().setEnabled(strEnabled));
    }

    @Test
    void desired() {
        assertThat(createDesired(false, false)).isFalse();
        assertThat(createDesired(false, true)).isTrue();
        assertThat(createDesired(true, false)).isTrue();
        assertThat(createDesired(true, true)).isTrue();
    }

    @Test
    void options() {
        final var cleanStart = false;
        final var autoReconnect = false;
        final var username = "user-name";
        final var password = "pass-word";
        final var config = new MqttConfig()
                .setCleanStart(cleanStart)
                .setAutoReconnect(autoReconnect)
                .setUsername(username)
                .setPassword(password);

        final var willText = "I am dead!";
        final var willTopic = "will/topic";
        final var willMessage = new MqttMessage(willText.getBytes(StandardCharsets.UTF_8));
        final var composer = mock(Composer.class);
        when(composer.willMessage()).thenReturn(new Composer.Bundle(willTopic, willMessage));

        final var creator = new Creator();

        final var options = creator.mqttConnectionOptions(config, composer);
        assertThat(options.isCleanStart()).isEqualTo(cleanStart);
        assertThat(options.isAutomaticReconnect()).isEqualTo(autoReconnect);

        assertThat(options.getRequestResponseInfo()).isTrue();
        assertThat(options.getRequestProblemInfo()).isTrue();

        assertThat(options.getUserName()).isEqualTo(username);
        assertThat(options.getPassword()).isEqualTo(password.getBytes(StandardCharsets.UTF_8));

        assertThat(options.getWillDestination()).isEqualTo(willTopic);
        assertThat(options.getWillMessage()).isEqualTo(willMessage);
    }

    @Test
    void optionsEmpty() {
        final var config = new MqttConfig();
        final var composer = mock(Composer.class);

        final var creator = new Creator();

        final var options = creator.mqttConnectionOptions(config, composer);
        assertThat(options.isCleanStart()).isEqualTo(config.isCleanStart());
        assertThat(options.isAutomaticReconnect()).isEqualTo(config.isAutoReconnect());

        assertThat(options.getRequestResponseInfo()).isTrue();
        assertThat(options.getRequestProblemInfo()).isTrue();

        assertThat(options.getUserName()).isNull();
        assertThat(options.getPassword()).isNull();

        assertThat(options.getWillDestination()).isNull();
        assertThat(options.getWillMessage()).isNull();
    }

    @Test
    void clientBroken() {
        final var config = new MqttConfig().setServerUri("ftp://example.net");
        final var creator = new Creator();

        assertThatExceptionOfType(BeanInstantiationException.class)
                .isThrownBy(() -> creator.mqttAsyncClient(config))
                .withMessageContaining("creation failed");
    }

    @Test
    void client() {
        final var serverUri = "tcp://localhost:1312";
        final var clientId = "test-client";

        final var config = new MqttConfig().setServerUri(serverUri).setClientId(clientId);
        final var creator = new Creator();

        final var client = creator.mqttAsyncClient(config);

        assertThat(client.isConnected()).isFalse();
        assertThat(client.getServerURI()).isEqualTo(serverUri);
        assertThat(client.getClientId()).isEqualTo(clientId);
    }
}
