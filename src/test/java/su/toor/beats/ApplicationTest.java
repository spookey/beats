package su.toor.beats;

import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.Mockito.mockStatic;

import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.boot.SpringApplication;

class ApplicationTest {

    private static final String[] ARGS = new String[] {"some", "args"};

    @Test
    void success() {
        try (final MockedStatic<SpringApplication> mockedStatic = mockStatic(SpringApplication.class)) {

            assertThatNoException().isThrownBy(() -> Application.main(ARGS));

            mockedStatic.verify(() -> SpringApplication.run(Application.class, ARGS));
        }
    }
}
