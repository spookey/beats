import SockJS from 'sockjs-client';
import Stomp from 'webstomp-client';

export default class Client {
  constructor(endpoint) {
    this.inst = Stomp.over(new SockJS(endpoint), {
      protocols: ['v12.stomp'],
      binary: false,
      heartbeat: false,
      debug: false,
    });
  }

  run(topic, consumer) {
    this.inst.connect(
      {},
      (frame) => {
        if (frame) {
          console.log(frame);
        }
        this.inst.subscribe(topic, (tick) => {
          consumer(JSON.parse(tick.body));
        });
      },
      (err) => {
        if (err) {
          console.error(err);
        }
        consumer(null);
      },
    );
  }
}
