import { describe, expect, test } from 'vitest';

import { shallowMount } from '@vue/test-utils';
import InternetTime from '@/components/InternetTime.vue';

describe('InternetTime.vue', () => {
  const uuid = 'random-UUID';
  Object.defineProperty(window, 'crypto', {
    value: { randomUUID: () => uuid },
  });

  test('has initial defaults', () => {
    const wrapper = shallowMount(InternetTime, {
      propsData: { value: null, grain: 0 },
    });

    const head = wrapper.get('h1');
    expect(head.text()).toBe('…');

    const prog = wrapper.get('progress');
    expect(prog.element.value).toBe(0);
    expect(prog.element.max).toBe(1000);
  });

  test('adds label for progress', () => {
    const wrapper = shallowMount(InternetTime, {
      propsData: { value: null, grain: 0 },
    });

    expect(wrapper.vm.progressId).toBe(uuid);

    const prog = wrapper.get('progress');
    expect(prog.element.id).toBe(uuid);

    const plbl = wrapper.get('label');
    expect(plbl.attributes().for).toBe(uuid);
    expect(plbl.classes()).toContain('is-hidden');
  });

  test('computes correctly', async () => {
    const wrapper = shallowMount(InternetTime, {
      propsData: { value: null, grain: 0 },
    });

    expect(wrapper.vm.current).toBe('…');
    expect(wrapper.vm.progress).toBeNull();

    await wrapper.setProps({ value: 23.42 });
    expect(wrapper.vm.current).toBe('@023');
    expect(wrapper.vm.progress).toBe(23);

    await wrapper.setProps({ grain: 1 });
    expect(wrapper.vm.current).toBe('@023.4');
    expect(wrapper.vm.progress).toBe(23);

    await wrapper.setProps({ grain: 2 });
    expect(wrapper.vm.current).toBe('@023.42');
    expect(wrapper.vm.progress).toBe(23);

    await wrapper.setProps({ grain: 3 });
    expect(wrapper.vm.current).toBe('@023.420');
    expect(wrapper.vm.progress).toBe(23);

    await wrapper.setProps({ grain: 4 });
    expect(wrapper.vm.current).toBe('@023');
    expect(wrapper.vm.progress).toBe(23);
  });

  test('switches output format', async () => {
    const wrapper = shallowMount(InternetTime, {
      propsData: { value: 23.42, grain: 0 },
    });

    expect(wrapper.get('h1').text()).toBe('@023');

    await wrapper.setProps({ grain: 1 });
    expect(wrapper.get('h1').text()).toBe('@023.4');

    await wrapper.setProps({ grain: 2 });
    expect(wrapper.get('h1').text()).toBe('@023.42');

    await wrapper.setProps({ grain: 3 });
    expect(wrapper.get('h1').text()).toBe('@023.420');

    await wrapper.setProps({ grain: 4 });
    expect(wrapper.get('h1').text()).toBe('@023');
  });

  test('updates the value', async () => {
    const wrapper = shallowMount(InternetTime, {
      propsData: { value: 23, grain: 0 },
    });

    const oHead = wrapper.get('h1');
    expect(oHead.text()).toBe('@023');

    const oProg = wrapper.get('progress');
    expect(oProg.element.value).toBe(23);

    await wrapper.setProps({ value: 42 });

    const nHead = wrapper.get('h1');
    expect(nHead.text()).toBe('@042');

    const nProg = wrapper.get('progress');
    expect(nProg.element.value).toBe(42);
  });
});
