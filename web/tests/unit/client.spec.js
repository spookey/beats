import { describe, expect, test } from 'vitest';

import Client from '@/client';

const ENDPOINT = '/web-socket';
const TOPIC = '/some/weird/topic';

describe('client.js', () => {
  test('constructs', () => {
    const client = new Client(ENDPOINT);

    expect(client.inst).not.toBeUndefined();

    const { inst } = client;

    expect(inst.connected).toBeFalsy();
    expect(inst.isBinary).toBeFalsy();
    expect(inst.hasDebug).toBeFalsy();
    expect(inst.heartbeat.incoming).toBe(0);
    expect(inst.heartbeat.outgoing).toBe(0);
    expect(inst.subscriptions).toStrictEqual({});
    expect(inst.protocols).toStrictEqual(['v12.stomp']);
    expect(inst.ws.url).toBe(`http://localhost:3000${ENDPOINT}`);
  });

  test('runs', () => {
    const data = { some: 'data' };
    const trace = {
      headers: null,
      topic: null,
      payload: null,
      onConnect: null,
      onTick: null,
      onError: null,
    };

    const client = new Client(ENDPOINT);
    client.inst.connect = (headers, onConnect, onError) => {
      trace.headers = headers;
      trace.onConnect = onConnect;
      trace.onError = onError;
    };
    client.inst.subscribe = (topic, onTick) => {
      trace.topic = topic;
      trace.onTick = onTick;
    };

    const consumer = (payload) => { trace.payload = payload; };
    client.run(TOPIC, consumer);

    expect(trace.headers).toStrictEqual({});
    expect(trace.topic).toBeNull();

    trace.onConnect();
    expect(trace.topic).toBe(TOPIC);
    expect(trace.payload).toBeNull();

    trace.onTick({ body: JSON.stringify(data) });
    expect(trace.payload).toStrictEqual(data);

    trace.onError();
    expect(trace.payload).toBeNull();
  });
});
