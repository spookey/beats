import { describe, expect, test } from 'vitest';

import { shallowMount } from '@vue/test-utils';
import App from '@/App.vue';
import InternetTime from '@/components/InternetTime.vue';
import Client from '@/client';

describe('App.vue', () => {
  test('includes component', () => {
    const wrapper = shallowMount(App, {});

    const internetTime = wrapper.findComponent(InternetTime);
    expect(internetTime.exists()).toBe(true);
  });

  test('has initial defaults', () => {
    const wrapper = shallowMount(App, {});

    expect(wrapper.vm.client).toBeInstanceOf(Client);
    expect(wrapper.vm.value).toBeNull();
    expect(wrapper.vm.grain).toEqual(0);
  });

  test('updates values', () => {
    const numPayload = 23.42;
    const nilPayload = 0;

    const wrapper = shallowMount(App, {});

    wrapper.vm.update(numPayload);

    expect(wrapper.vm.value).toBe(numPayload);

    wrapper.vm.update(nilPayload);

    expect(wrapper.vm.value).toBe(nilPayload);

    wrapper.vm.update(undefined);

    expect(wrapper.vm.value).toBeNull();

    wrapper.vm.update(null);

    expect(wrapper.vm.value).toBeNull();
  });
});
